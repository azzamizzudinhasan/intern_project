package com.investree.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="payment_history")
public class Payment_history {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="pembayaran_ke")
    private Integer pembayaran_ke;

    @Column(name="jumlah")
    private Double jumlah;

    @Column(name="bukti_pembayaran", length = 150)
    private String bukti_pembayaran;

    @ManyToOne(targetEntity = Transaksi.class, cascade = CascadeType.ALL)
    @JoinColumn(name="id_transaksi", referencedColumnName = "id")
    private Transaksi transaksi;
}
