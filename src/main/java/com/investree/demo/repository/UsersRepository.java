package com.investree.demo.repository;

import com.investree.demo.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;

@Repository
public interface UsersRepository extends PagingAndSortingRepository<Users, Long> {

    @Query("SELECT c FROM Users WHERE c.id = :id")
    public Users getById(@Param("id") Long id);

    @Query("SELECT c FROM Users")
    Page<Users> getAllData(Pageable pageable);
}
