package com.investree.demo.repository;

import com.investree.demo.model.Payment_history;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.awt.print.Pageable;

public interface Payment_historyRepository extends PagingAndSortingRepository<Payment_history, Long> {

    @Query("SELECT c FROM Payment_history WHERE c.id = :id")
    public Payment_history getById(@Param("id") Long id);

    @Query("SELECT c FROM Payment_history")
    Page<Payment_history> getAllData(Pageable pageable);
}
