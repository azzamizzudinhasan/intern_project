package com.investree.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="transaksi")
public class Transaksi {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="tenor")
    private Integer tenor;

    @Column(name="total_pinjaman")
    private Double total_pinjaman;

    @Column(name="bunga_persen")
    private Double bunga_persen;

    @Column(name="status", length = 25)
    private String status;

    @OneToMany(mappedBy = "transaksi", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Payment_history> payment_history;

    @ManyToOne(targetEntity = Users.class, cascade = CascadeType.ALL)
    @JoinColumn(name="id_peminjam", referencedColumnName = "id")
    @JoinColumn(name="id_meminjam", referencedColumnName = "id")
    private Users user;
}
