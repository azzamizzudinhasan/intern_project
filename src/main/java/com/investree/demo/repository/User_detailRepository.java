package com.investree.demo.repository;

import com.investree.demo.model.User_detail;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.awt.print.Pageable;

public interface User_detailRepository extends PagingAndSortingRepository<User_detail, Long> {
    @Query("SELECT c FROM User_detail WHERE c.id = :id")
    public User_detail getById(@Param("id") Long id);

    @Query("SELECT c FROM User_detail")
    Page<User_detail> getAllData(Pageable pageable);
}
