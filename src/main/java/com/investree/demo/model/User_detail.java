package com.investree.demo.model;

import lombok.Data;
import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;

@Data
@Entity
@Table(name="user_detail")
public class User_detail {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nama", length = 25)
    private String nama;

    @Column(name="alamat", columnDefinition = "TEXT")
    private String alamat;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_user", referencedColumnName = "id")
    private Users user;
}
