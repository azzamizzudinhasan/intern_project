package com.investree.demo.view.imp;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.Payment_historyRepository;
import com.investree.demo.repository.TransaksiRepository;
import com.investree.demo.view.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class TransaksiPaymentImple implements TransaksiService {

    @Autowired
    public TransaksiRepository repoTransaksi;

    @Override
    public Map save(Transaksi transaksi){
        Map map = new HashMap();
        try{
            Transaksi save = repoTransaksi.save(transaksi);
            map.put("data", save);
            map.put("code", 200);
            map.put("status", "sukses");
            return map;
        }catch(Exception e){
            map.put("code", 500);
            map.put("status", "failed");
            return map;
        }
    }

    @Override
    public Map updateStatus(Transaksi transaksi){
        Map map = new HashMap();
        try{
            Transaksi update = repoTransaksi.getById(transaksi.getId());

            update.setStatus("Lunas");

            Transaksi dosave = repoTransaksi.save(update);
            map.put("data", dosave);
            map.put("code", 200);
            map.put("status", "sukses");
            return map;
        }catch(Exception e){
            map.put("code", 500);
            map.put("status", "failed");
            return map;
        }
    }
}
