package com.investree.demo.controller;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepository;
import com.investree.demo.view.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("v1/transaksi")
public class TransaksiController{

    @Autowired
    public TransaksiRepository repoTransaksi;

    @Autowired
    public TransaksiService serviceTransaksi;

    @PostMapping("")
    public ResponseEntity<Map> save(@RequestBody Transaksi transaksi){
        Map save = serviceTransaksi.save(transaksi);
        return new ResponseEntity<Map>(save, HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<Map> updateStatus(@RequestBody Transaksi transaksi){
        Map updateStatus = serviceTransaksi.updateStatus(transaksi);
        return new ResponseEntity<Map>(updateStatus, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Map> list(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam(required = false) String status ){
        Pageable show_data = PageRequest.of(page,size);
        Page<Transaksi> transaksiList = null;
        if(status != null){
            repoTransaksi.getByStatus(status, show_data);
        }else{
            repoTransaksi.findAll(show_data);
        }

        Map map = new HashMap();
        map.put("content",transaksiList);
        return new ResponseEntity<Map>(map, new HttpHeaders(), HttpStatus.OK);
    }
}
