package com.investree.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="users")
public class Users {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="username", length = 15)
    private String username;

    @Column(name="password", length = 25)
    private String password;

    @Column(name="is_active")
    private Boolean is_active;

    @OneToOne(mappedBy = "users")
    private User_detail user_detail;

    @OneToMany(mappedBy = "users", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaksi> transaksi;
}
